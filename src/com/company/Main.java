package com.company;

import info.debatty.java.stringsimilarity.Jaccard;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        ArrayList<String> arrayList = new ArrayList<>();

        try (Stream<String> stream = Files.lines(Paths.get("./src/com/company/io/input.txt"))) {
            stream.forEach(arrayList::add);
        } catch (IOException e) {
            System.out.println("File input.txt error.");
        }

        int n;
        try {
            n = Integer.parseInt(arrayList.get(0));
            arrayList.remove(0);
            arrayList.remove(n);
        } catch (IndexOutOfBoundsException e) {
            System.out.println("File input.txt empty or incorrect sequence of words.");
        }

        Jaccard jaccard = new Jaccard();
        HashMap<String, String> synonyms = getSynonyms();

        ArrayList<Boolean> isRelationFoundArray = new ArrayList<>(Arrays.asList(new Boolean[arrayList.size()]));
        Collections.fill(isRelationFoundArray, Boolean.FALSE);

        try (FileOutputStream fos = new FileOutputStream("./src/com/company/io/output.txt");
             OutputStreamWriter osw = new OutputStreamWriter(fos)) {
            String line = null;
            for (int i = 0; i < arrayList.size() - 1; i++) {
                if (isRelationFoundArray.get(i)) {
                    continue;
                }
                for (int j = i + 1; j < arrayList.size(); j++) {
                    line = arrayList.get(i);
                    if (isRelationFoundArray.get(j)) {
                        continue;
                    }
                    if ((jaccard.similarity(arrayList.get(i).toLowerCase(), arrayList.get(j).toLowerCase())) > 0.1) {
                        line += ":" + arrayList.get(j) + "\r\n";
                        isRelationFoundArray.set(i, true);
                        isRelationFoundArray.set(j, true);
                        break;
                    } else {
                        for (HashMap.Entry<String, String> entry : synonyms.entrySet()) {
                            if ((jaccard.similarity(arrayList.get(i).toLowerCase(), entry.getKey().toLowerCase()) > 0.1) &&
                                    (jaccard.similarity(arrayList.get(j).toLowerCase(), entry.getValue().toLowerCase()) > 0.1)) {
                                line += ":" + arrayList.get(j) + "\r\n";
                                isRelationFoundArray.set(i, true);
                                isRelationFoundArray.set(j, true);
                                break;
                            } else if ((jaccard.similarity(arrayList.get(i).toLowerCase(), entry.getValue().toLowerCase()) > 0.1) &&
                                    (jaccard.similarity(arrayList.get(j).toLowerCase(), entry.getKey().toLowerCase()) > 0.1)) {
                                line += ":" + arrayList.get(j) + "\r\n";
                                isRelationFoundArray.set(i, true);
                                isRelationFoundArray.set(j, true);
                                break;
                            }
                        }
                    }
                }
                if (!isRelationFoundArray.get(i)) {
                    line += ":?\r\n";
                }

                osw.append(line);
            }
            if (!isRelationFoundArray.get(arrayList.size() - 1)) {
                osw.append(arrayList.get(arrayList.size() - 1)).append(":?\r\n");
            }

        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO Error output.txt");
        }
    }

    private static HashMap<String, String> getSynonyms() {
        HashMap<String, String> synonyms = new HashMap<>();
        try (Stream<String> stream = Files.lines(Paths.get("./src/com/company/io/synonyms.txt"))) {
            stream.forEach(l -> synonyms.put(l.split(":")[0], l.split(":")[1]));
        } catch (IOException e) {
            System.out.println("Synonyms file error.");
        }
        return synonyms;
    }


}

